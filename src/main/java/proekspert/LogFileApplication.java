package proekspert;

import org.apache.commons.cli.*;
import proekspert.facade.LogFileFacade;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LogFileApplication {

    public static void main(String[] args) {
        long start = System.nanoTime();

        List<String> cli = Arrays.asList(args);
        LogFileFacade facade = new LogFileFacade();

        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption("f", "file", true, "File Path");
        options.addOption("l", "limit", true, "Resource Limit");
        options.addOption("h", "help", false, "Helper");

        try {
            CommandLine commandLine = parser.parse(options, args);

            if (commandLine.hasOption("h") || !commandLine.hasOption("f")) {
                HelpFormatter helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("CommandLineParameters", options);
            } else if (commandLine.hasOption("f")) {
                String path = commandLine.getOptionValue("f");
                Integer resourceLimit = commandLine.hasOption("l")
                        ? Integer.parseInt(commandLine.getOptionValue("l"))
                        : 10;
                facade.work(path, resourceLimit);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());;
        }

        long end = System.nanoTime();
        long total = TimeUnit.NANOSECONDS.toMillis(end - start);
        facade.displayWorkingTimeInMillis(total);
    }
}
