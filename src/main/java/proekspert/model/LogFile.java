package proekspert.model;

public class LogFile {

    private int hour;
    private String resource;
    private int requestDuration;

    public LogFile(int hour, String resource, int requestDuration) {
        this.hour = hour;
        this.resource = resource;
        this.requestDuration = requestDuration;
    }

    public LogFile() {
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public int getRequestDuration() {
        return requestDuration;
    }

    public void setRequestDuration(int requestDuration) {
        this.requestDuration = requestDuration;
    }
}
