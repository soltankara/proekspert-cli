package proekspert.service;

import proekspert.model.LogFile;
import proekspert.util.Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class LogFileService {

    // accepts String formatted file name, and returns list of string arrays which contain splitted lines
    public List<String[]> read(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        List<String[]> list = new ArrayList<>();

        Scanner scanner = new Scanner(file);
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            list.add(line.split(" |,"));
        }
        scanner.close();
        return list;
    }

    // accepts list of string arrays, and returns list of log file
    public List<LogFile> parse(List<String[]> fileLinesList) {
        List<LogFile> logFiles = new ArrayList<>();

        for (String[] fileLine : fileLinesList) {
            LogFile logFile = new LogFile();
            logFile.setHour(Parser.stringToLocalDate(fileLine[0] + " " + fileLine[1]).getHour());
            logFile.setResource(fileLine[5].split("\\?")[0]);
            logFile.setRequestDuration(Integer.parseInt(fileLine[fileLine.length - 1]));
            logFiles.add(logFile);
        }
        return logFiles;
    }

    // updates the map. if a resource (key) has list of request durations then update the list else put new item into map
    public void updateRequestDurationsOfResources(Map<String, List<Integer>> averageRequestTime, LogFile logFile) {
        if (averageRequestTime.containsKey(logFile.getResource())) {
            averageRequestTime.get(logFile.getResource()).add(logFile.getRequestDuration());
        } else {
            List<Integer> requestDurations = new ArrayList<>();
            requestDurations.add(logFile.getRequestDuration());
            averageRequestTime.put(logFile.getResource(), requestDurations);
        }
    }

    // map contains resource name as key, and list of all request durations as value.
    // returns new map which contain resource name as a key, and average request duration as a value
    public Map<String, Double> filterAverageRequestTime(Map<String, List<Integer>> averageRequestTime) {
        Map<String, Double> filteredAverageRequestTime = new HashMap<>();

        for (Map.Entry<String, List<Integer>> set : averageRequestTime.entrySet()) {
            Double averageDuration = calculateAverage(set.getValue());
            filteredAverageRequestTime.put(set.getKey(), averageDuration);
        }
        return filteredAverageRequestTime;
    }

    // calculate the average of the given list
    private Double calculateAverage(List<Integer> requestDurations) {
        double duration = 0.0;

        for (Integer requestDuration : requestDurations) {
            duration += requestDuration;
        }
        return duration / requestDurations.size();
    }

    // update map. if given item exist, update its value, else put as new item. Map only contains hours which have requests
    public void updateHourlyNumberOfRequests(Map<Integer,Integer> hourlyNumberOfRequests, LogFile logFile) {
        if (hourlyNumberOfRequests.containsKey(logFile.getHour())) {
            hourlyNumberOfRequests.put(logFile.getHour(), hourlyNumberOfRequests.get(logFile.getHour()) + 1);
        } else {
            hourlyNumberOfRequests.put(logFile.getHour(), 1);
        }
    }

    // display hourly requests
    public void displayHourlyRequests(Map<Integer,Integer> hourlyNumberOfRequests) {
        System.out.println("**********************************************************************************");
        System.out.println("**                     Histogram of Hourly Requests                             **");
        System.out.println("**********************************************************************************");

        for (Map.Entry<Integer, Integer> set : hourlyNumberOfRequests.entrySet()) {
            System.out.println("Hour: " + set.getKey() + " -> Request Amount: " + set.getValue());
        }

        System.out.println("**********************************************************************************");
        System.out.println("**                  Non displayed hours do not have requests                    **");
        System.out.println("**********************************************************************************");
    }



    // display average request durations of resources
    public void displayAverageRequestDuration(Map<String, Double> averageRequestTime, int limit){
        System.out.println("**********************************************************************************");
        System.out.println("**                Resources with highest average request duration               **");
        System.out.println("**********************************************************************************");

        for (Map.Entry<String, Double> set : getAverageRequestTime(averageRequestTime, limit).entrySet()) {
            System.out.println("Resource Name: " + set.getKey() + " -> Average Request Duration " + set.getValue());
        }
        System.out.println("**********************************************************************************");
    }

    // sort map, and return with given size (limit)
    private Map<String, Double> getAverageRequestTime(Map<String, Double> averageRequestTime, int limit) {
        return averageRequestTime.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(limit)
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }
}
