package proekspert.facade;

import proekspert.model.LogFile;
import proekspert.service.LogFileService;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogFileFacade {

    private LogFileService service = new LogFileService();

    public void work(String fileName, int resourceLimit) {
        List<String[]> lines = null;
        try {
            lines = service.read(fileName);
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found. Please, check file path.");
            return;
        }
        List<LogFile> logFiles = service.parse(lines);

        Map<String, List<Integer>> averageRequestTime = new HashMap<>();
        Map<Integer, Integer> hourlyNumberOfRequests = new HashMap<>();

        logFiles.stream().forEach(logFile -> {
            service.updateRequestDurationsOfResources(averageRequestTime, logFile);
            service.updateHourlyNumberOfRequests(hourlyNumberOfRequests, logFile);
        });

        service.displayAverageRequestDuration(service.filterAverageRequestTime(averageRequestTime), resourceLimit);
        service.displayHourlyRequests(hourlyNumberOfRequests);
    }

    public void displayWorkingTimeInMillis(long total) {
        System.out.println("-----------------------------------------------------");
        System.out.println("Application running time is: " + total + " millis");
        System.out.println("-----------------------------------------------------");
    }

    public void displayHelpText() {
        System.out.println("--------------------------------------------------------------------------------------------------");
        System.out.println("|  Log file path and amount of resources which needs to be shown should be appended run command  |");
        System.out.println("|  Example: java -jar <application-name.jar> <log-file-path> <resource-amount>                   |");
        System.out.println("--------------------------------------------------------------------------------------------------");
    }

}
